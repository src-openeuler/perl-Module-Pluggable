Name:                perl-Module-Pluggable
Epoch:               2
Version:             6.2
Release:             2
Summary:             Automatically give your module the ability to have plugins
License:             GPL-1.0-or-later OR Artistic-1.0-Perl
URL:                 https://metacpan.org/release/Module-Pluggable
Source0:             https://cpan.metacpan.org/authors/id/S/SI/SIMONW/Module-Pluggable-%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       coreutils findutils make perl-interpreter perl-generators
BuildRequires:       perl(ExtUtils::MakeMaker) perl(File::Spec::Functions) >= 3.00 perl(strict)
BuildRequires:       perl(warnings) perl(Carp) perl(deprecate) perl(Exporter) >= 5.57
BuildRequires:       perl(File::Basename) perl(File::Find) perl(if) perl(vars)
BuildRequires:       perl(Module::Runtime) >= 0.012 perl(base) perl(Data::Dumper) perl(FindBin)
BuildRequires:       perl(lib) perl(Test::More) >= 0.62
Requires:            perl(File::Spec::Functions) >= 3.00 perl(deprecate)
Recommends:          perl(Module::Runtime) >= 0.012

%description
Provides a simple but, hopefully, extensible way of having 'plugins' for your module.
obviously this isn't going to be the be all and end all of solutions but is works for me
Essentially all it does is export a method into your namespace that looks.
through a search path for .pm files and turn those into class names.
Optionally it instantiates those classes for you.

%prep
%autosetup -n Module-Pluggable-%{version} -p1
find -type f -exec chmod -x {} +

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:6.2-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Nov 07 2024 shenzhongwei <shenzhongwei@kylinos.cn> - 2:6.2-1
- Update to version 6.2
- Fix typo in examples
- Fix spelling of "FatPacker"
- Add before_instantiate and after_instantiate hooks
- Use explicit test plan instead of done_testing
- Skip unreadable files
- Fix behaviour of File::Find and symlinks on certain versions of Win32
- Improve vim swapfile patterns
- Ignore the .AppleDouble
- Don't use vars, use our

* Fri Apr 24 2020 yanan li <liyanan032@huawei.com> - 2:5.2-10
- package init
